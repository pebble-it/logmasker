# LogMasker

A simple utility that masks linux log files so that you can safely share the files with an external party. LogMasker can mask values with replacements of the same format so that external parties are unaware you have masked the file. When they ask about a particular device/user/IP, etc... then you can use a mapping file to see the original value they wanted to discuss so that you can investigate further.

## What does LogMasker do ?
1. It takes a source text file (usually a log file but can be any text file) and will change the contents of that file based on the masking rules you ask it to use. 
2. It finds all occurrences of qualifying text and uses your masking rules to specify a unique value for each unique entry. This uniqueness then allows you to build a mapping from old to new. LogMasker can substitute the regex replacement expression with user-defined randomisation for each unique occurrence that is found by the qualifying regex search criteria. For example, if the regex search criteria results in 100 different matches (as would likely be for regex rules that search for IP Addresses), then each entry will then be able to have its own unique corresponding masked value. 
3. LogMasker will re-build any mask values that already exist in the log file or as other masked values so that the new logfile is unique. Note that you can specify for LogMasker to ignore this aspect, otherwise it will guarantee uniqueness or fail.
4. LogMasker can either overwrite the existing logfile or create a new logfile in the location specified with the name specified.
5. If all the entries are unique and you have requested a mapping file, LogMasker will then create a mapping file for you using the name and location specified.

## How does LogMasker make masked entries unique ?
This is the secret sauce. The masking rules allow you to specify both the search criteria and the replacement criteria. Both use standard Perl-compatible regular expressions (https://docs.python.org/3/howto/regex.html) but with capability that can result in a unique value being generated for every match found by the search criteria. This is done by specifying randomization in your replacement criteria:
- Random lowercase letter(s) with an option range of from/to letters allowed.
- Random uppercase letters(s) with an option range of from/to letters allowed.
- Random integer(s) with an option range of from/to numbers allowed.
- Random selection of a provided list of strings that can include any combination of text, integers, punctuation and space.

For example, if you had regex criteria that found all server names then you could specify a replacement specification of:
- &RAN['saturn', 'venus', 'pluto', 'krypton' 'mars', uranus']-&RAN(N1,8,9)&RAN(T1,T,Z)&RAN(N3).
- This could result in values of 'krypton-8T537', 'uranus-9X004', etc... and these would map back to the original server names so that if you are working with an external support representative and they were asking about the 'krypton-8T537' server, you new in fact that this was in fact 'SAP_PROD01'. Every occurrence of SAP_PROD01 used in the regex criteria specified will be substituted with the krypton replacement. Note that uranus-7... would not be possible because that violates the range of 8,9 specified. Ranges can be used for single integers and single letters.

The randomization resolves to a unique regex replacement criteria specified for each matching value.

## Requirements

- Python 3.6
- Linux 

A windows compliant version is in the works, but no ETA yet.

## Masks

LogMasker ships with a directory of sample masking files that have common formats of sensitive data known to exist in these files. Use these sample masking files to create your own. Ensure not to modify the original as it will get overwritten on subsequent pull requests.


## How-To 
### Overwrite a log file with masked entries and create a mapping file for lookups
To overwrite the existing log file you do not specify a new logfile name and it will overwrite the existing log.

```
python3 logmasker.py --logfile /path/to/logfile.log --mask masks/all_masks.masks --map /create/this/here/logfile.map --out screen
```

### Create a new masked logfile but without a corresponding mapping file
You must specify a new logfile name but you do not specify a mapping file to create
```
python3 logmasker.py --log /path/to/logfile.log --mask masks/all_masks.masks --newlog /create/this/here/newlogfile.log --out screen
```

### Test that a new regex expression will match against entries in a logfile
Use an online python-regex service to help build the regex search pattern and replacement expression. Then substitute with &RAN (random) value generators and parse it to LogMasker as lines rather than files
```
python3 logmasker.py -L 'Jun 18 02:08:10 combo ftpd[31274]: connection from 10.610.92.204' \
                     -M "'[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}', '10.&RAN(N3).&RAN(N2).&RAN(N3)'" --out screen
```

### Localise the behaviour of LogMasker
Non-Python developers can use any text editor to open the logmasker.py file and edit the 'LOCALISATIONS' section at the start of the file. All instructions are within the file on localisation values that can be used instead of the default values provided. Localisations can include what timezone to use in log files, whether informational comments are inserted in the masked logfile and regex flags to apply to all masking expressions (these are some, there are many more).

## Build masking rules
LogMasker contains several templates of regex criteria. You can create your own masking files or use the standard templates provided. For optimal performance, it is recommended that you only specify the rules that are known to apply to particular log sources. If performance is not of a concern, then all rules could be placed in a single file. Be careful to make your own copies of the LogMasker templates as they will be overwritten by subsequent pull requests. The Masking rule files allow commenting so that you can document your entries and easily enable/disable existing entries as required. 

## Performance
Obviously this will be determined by the available resources where LogMasker is run. Large logfiles are loaded into memory, but processed in chunks that can vary according to parameter (default is 10MB). Testing reveals that LogMasker can process 10MB of logfile per second with 10 masking rules. If you do have extremely large logfiles (for example, > 1 GB), consider breaking the logfile into smaller chunks and calling logMasker in multiple processes, then join all resulting files to form a new single masked logfile. If this is done, ensuring you break the chunk on a newline so that the complete line is searched for masking criteria.

## Documentation

[Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)


## Roadmap
We are considering the following enhancements in the following order:
- Migrate file operations to mmap for search but not replace and file write
- An option to embed the mapping file to the logfile in an unreadable format that does not interfere with the logfile
- Use of C to process high-performance replacements in extremely large logfiles (> 100MB)
- Wrapper for submitting a directory of files to mask
- Wrapper for submitting a string to mask, either with a mapfile or a string mapping entry. This could also be used for testing your mapping entries
- Wrapper for an API call returning a string 
- Windows Server compatability
- REST API capability
- Restoring to original logfile
- Multiprocess/thread capability for processing extremely large logfiles (> 1 TB)

## Contributing
We welcome mask files that contain masking rules for particular types of logfiles. We may make your contributions into a new mask file or add your entries into another mask file.

If there is a particular generic feature that you believe would be strategic for LogMasker, please contact us to discuss further.

## License
LogMasker is licensed under GPL v3 which is incluced at https://gitlab.com/pebble-it/logmasker/-/raw/main/LICENSE 
The GPL v3 license text can be found at https://www.gnu.org/licenses/lgpl-3.0.txt

## Project status
STILL DRAFT -- do not use in anger just yet :)
