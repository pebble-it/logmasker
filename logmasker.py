# logmasker.py
# Processes a logfile using a maskfile to create a new newlogfile
# Produces a mapfile to allow a user to perform a lookup of old v new
#
# License: Logmasker is licensed under GLP v3.0 --> https://gitlab.com/pebble-it/logmasker/-/raw/main/LICENSE
#
#          This program is free software: you can redistribute it and/or modify
#          it under the terms of version 3 the GNU General Public License as published by
#          the Free Software Foundation.
#
#          This program is distributed in the hope that it will be useful,
#          but WITHOUT ANY WARRANTY; without even the implied warranty of
#          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#          GNU General Public License for more details.
#
#          You should have received a copy of the GNU General Public License
#          along with this program.  If not, see <https://www.gnu.org/licenses/lgpl-3.0.txt>.
#
#
# Code: https://gitlab.com/pebble-it/logmasker/
# Docs: _______
#
# Usage:
#    python3 logmasker.py --help
#

import os
import sys
import traceback
import re
import argparse
import random
import json
from ast import literal_eval as ast_literal_eval
from datetime import datetime
DEBUG_INFO, DEBUG_MSG, DEBUG_ERR, DEBUG_NONE = 0, 5, 8, 9

##########################################################################
## LOCALISATIONS
##
## These global variables allow you to change the behaviour of LogMasker
## without having to change the code itself. Note that these will be
## overridden in future updates, so maintain a copy to re-apply to
## later releases
##
##########################################################################

DEBUG =      True               # True|False. If True, the --debug parameter is ignored and debug output will be produced
LOCAL_TIME = datetime.now()     # Any datetime representation including datetime.utcnow() to have UTC time, datetime.now(datetime.timezone(datetime.timedelta(hours=-?)))
UTC_TIME =   datetime.utcnow()  # This could be set to LOCAL_TIME or any other relative time as above
LOG_TIME =   UTC_TIME.strftime('%Y-%m-%d %H:%M:%S %z')   # Any format as documented in https://strftime.org

DEBUG_FILE =      f"LM-{LOCAL_TIME.strftime('%m%d-%H%M-%S%f')}.debug"  # Any unique legal filename is allowed.
DEBUG_DIR =       "~/lm_debug"  # If none, this will be placed in the same directory as (a) mapping file if provided; or (b) newlogfile if provided; or (c) $HOME.
DEBUG_LEVEL =     DEBUG_INFO             # Change to any DEBUG_KEY (listed near top of the file)
DEBUG_OUT_PREFIX = {DEBUG_INFO:'*INFO: ', # This is the prefix output for the messages in the debug file.
                    DEBUG_MSG: 'DEBUG: ', # Do not change the DEBUG_KEY, only the prefix text
                    DEBUG_ERR: 'ERROR: '
}
CHUNK_MB =        10            # The number of MB per chunk of source logfile being searched and replaced for masking. Any integer from 1 to 900 is allowed (100+ recommended for Fast Replace)
CHUNK_PCT =       40            # This is the percent of the final chunk compared to the prior chunk of source logfile being read into memory. Any number between 0 and 60 is allowed
ALLOWED_FLAGS = {'0': 0,        # Legal first character values that can be specified for REGEX Flags. Comment flag entries out if particular values are not to be allowed
                 'A': re.ASCII,
                 'I': re.IGNORECASE,
                 #'N': re.NOTHING,   an example of commenting out a flag value that is not allowed to be specified in the masking file
                 'L': re.LOCALE,
                 'V': re.VERBOSE,
                 'X': re.VERBOSE,   # Refer to https://docs.python.org/3/howto/regex.html#compilation-flags  for more information
}
CONSTANT_FLAGS = []                 # 'I', 'X', etc... all masking regex's will inherit these flags regardless of the mapping file. Mapping file can specify additions
CONSTANT_COMMENT_CHAR = "#"         # Any character(s) that uniquely identify that the line is considered a comment line. Used for comment lines in new logfiles, mapping files and debug files.
                                    # Can be overridden by parameters

CONSTANT_SEPERATOR_DEFAULT = "::::" # Sets the default value for the --seperator parameter. Use this constant value for seperating original v masked in the mapping output file
                                    # Can be overridden by parameters

ALLOW_LOGFILE_DELETE = True         # If set to False, the parameter --deletelog will be ignored and the logfiles will never be deleted
ALLOW_HEADERS =        True         # If set to False, the parameter --nofileheaders will be ignored and LogMasker comments will not be inserted into the log files

TEMP_LOGFILENAME =    f".TEMP-LOGFILE-{LOCAL_TIME.strftime('%M%S%f')}.temp"  # You can change this to a filename that will resolve to a unique filename. Only used when overriding existing log files
                                                                             # Note that it will get created in the DEBUG_DIR defined above
TIMEOUT_SECONDS =      60          # Return a STATUS_BAD_TIMEOUT if processing is longer than this number. Set to 0 if you want indefinite processing.

# Status Return codes (used HTTP response codes as a basis - https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)
STATUS_OK              = 200
STATUS_BAD_REQUEST     = 400
STATUS_BAD_PERMISSIONS = 403
STATUS_FILE_NOT_FOUND  = 404
STATUS_BAD_MAPPINGS    = 422
STATUS_NO_SOURCE       = 427
STATUS_BAD_EXCEPTION   = 500   # Not implemented
STATUS_BAD_TIMEOUT     = 504

##########################################################################
## END OF LOCALISATIONS
##########################################################################


##########################################################################
# GLOBALS
##########################################################################

RTN = {
    'status':         STATUS_OK,
    'logline_out':    None,
    'newlogfile':     None,
    'mapfile':        None,
    'debugfile':      None,
    'debugmapfile':   None,
    'mappings':       0,
    'masklines_zero': 0,
    'masklines_used': 0,
    'masklines_bad':  0,
    'exceptions':     []
}

START_TIME = datetime.utcnow()
ENABLE_RESTORE = False
RE_FLAGS_ID = '___re_flags_'
CHUNKCHARS = 0
MB1 = 1024*1024


##########################################################################
##
## FUNCTIONS
##
##########################################################################

##########################################################################
# DPRINT - Selective OUT messaging
##########################################################################

def dprint(msg, level=0):
    """ selectively output messages """

    if level >= DEBUG_LEVEL:
        with open(DEBUG_FILE, 'a') as file:
            file.write(f"{DEBUG_OUT_PREFIX.get(level, ' ')}{msg}\n")



##########################################################################
# FLATTENLIST - Flat a list of different types into a single list of values
##########################################################################

def flattenlist (old):
    """ convert input of list containing strings, tuples, sets and other lists into a list
                ['abc', ('def', 'hij', 'klm'), ['xx', {'yy', 'zz'}, 23], ('nop', 'qrs')]
        becomes ['abc', 'def', 'hij', 'klm', 'xx', 'yy', 'zz', '23', 'nop', 'qrs']
    """
    newlist = []
    for x in old:
        if len(x) > 0:
            if type(x) is set:
                x = list(x)

            if type(x) is tuple:
                for t in x:
                    if len(t) > 0:
                        newlist.append(str(t))
            elif type(x) is list:
                new_x = flattenlist(x)
                for nx in new_x:
                    if len(nx) > 0:
                        newlist.append(str(nx))
            else:
                newlist.append(str(x))

    return newlist



##########################################################################
# IS_TIMEOUT - Returns a False if processing within timeout, else True
##########################################################################

def is_timeout(msg=""):
    """ Calculate whether timeout variable exceeded and return boolean """

    if TIMEOUT_SECONDS is None or TIMEOUT_SECONDS < 1: return False

    current_time = datetime.utcnow()
    running_secs = int((current_time - START_TIME).total_seconds())

    if running_secs < TIMEOUT_SECONDS:
        if DEBUG_LEVEL <= DEBUG_MSG:
            dprint(f"is_timeout() returning False because {running_secs} < {TIMEOUT_SECONDS}, msg={msg}", DEBUG_MSG)
        return False
    else:
        timeout_msg = f"Timeout has occurred at {running_secs} seconds (TIMEOUT_SECONDS={TIMEOUT_SECONDS})"
        if len(msg) > 0: timeout_msg += f", msg='{msg}'"
        dprint(timeout_msg, DEBUG_ERR)
        RTN['status'] = STATUS_BAD_TIMEOUT
        RTN['exceptions'].append(timeout_msg)
        return True



##########################################################################
# REPLACE_LINES - Replace lines with nominated text in addition to leading and trailing lines
##########################################################################
def replace_lines(src, new_line='', leading_lines=0, trailing_lines=0):

    RTN['exceptions'].append('function replace_lines called')
    return src

    # Not yet coded

    # Split the string into a list of lines
    lines = src.splitlines()

    # Initialize a flag to keep track of whether we are inside a block
    inside_block = False

    # Initialize a variable to store the lines inside the block
    block_lines = []

    # Loop through each line in the list
    for line in lines:

        # Check if the word is present in the line
        if 'particular_word' in line:

            # If we are not already inside a block, set the flag to True and reset the block_lines variable
            if not inside_block:
                inside_block = True
                block_lines = []

            # Add the current line to the block_lines variable
            block_lines.append(line)

        # If the word is not present in the line and we are inside a block, remove the leading 5 lines and trailing 8 lines
        elif inside_block:
            block_lines = block_lines[leading_lines:trailing_lines*-1]

            # Write the modified block of lines to the output file
            output_file.write('\n'.join(block_lines) + '\n')

            # Reset the flag and the block_lines variable
            inside_block = False
            block_lines = []

    # If we reach the end of the file and we are still inside a block, remove the leading 5 lines and trailing 8 lines
    if inside_block:
        block_lines = block_lines[leading_lines:trailing_lines*-1]

        # Write the modified block of lines to the output file
        output_file.write('\n'.join(block_lines) + '\n')



##########################################################################
# QUIT - Safely exits the program
##########################################################################

def quit(out="screen", exit_code=-1):
    """ Returns the result and exits the program """

    dprint("quit called", DEBUG_INFO)

    exceptions = "\n"
    if len(RTN['exceptions']) > 0:
        exit_code = -1
        RTN['status'] = STATUS_BAD_REQUEST if RTN['status'] == STATUS_OK else RTN['status']
        dprint("*** The following exceptions have been encountered:\n  " + "\n  - ".join(RTN['exceptions']), 8)

        if DEBUG_LEVEL < DEBUG_ERR and out == 'screen':
            for x in RTN['exceptions']:
                exceptions += f"  - {x}\n"

            exceptions += "\n"


    out_val = None
    if out in ('json', 'stdout'):
        out_val = json.dumps(RTN)
    elif out == 'screen':
        current_time = datetime.utcnow()
        running_secs = f"\n\nTotal time: {int((current_time - START_TIME).total_seconds())} seconds\n"
        out_val =                                           f"\n LogMasker Return Status: {RTN['status']}" + \
                    ("" if RTN['logline_out'] is None else  f"\n Masked Log Line:         {RTN['logline_out']}") + \
                    ("" if RTN['newlogfile'] is None else   f"\n Masked Log File:         {RTN['newlogfile']}") + \
                    ("" if RTN['mapfile'] is None else      f"\n Mapping File:            {RTN['mapfile']}") + \
                                                            f"\n Unique Masked Values:    {RTN['mappings']}" + \
                                                            f"\n Zero Match Map Lines:    {RTN['masklines_zero']}" + \
                                                            f"\n Matching Map Lines:      {RTN['masklines_used']}" + \
                                                            f"\n Bad Map Lines:           {RTN['masklines_bad']}" + \
                    ("" if RTN['debugfile'] is None else    f"\n Debug File:              {RTN['debugfile']}") + \
                    ("" if RTN['debugmapfile'] is None else f"\n Debug Mapping File:      {RTN['debugmapfile']}") + \
                                                            f"\n Exceptions:              {len(RTN['exceptions'])}\n{exceptions}{running_secs}"

    if out_val is not None:
        sys.stdout.write(out_val)
        sys.stdout.flush()

    sys.exit(exit_code)


##########################################################################
# RRAN - Return a Random value
##########################################################################

def rran(src):
    """
    Build a random value of either integers, lowercase text, uppercase text or a string from a list
    """
    if src is None:
        src = ""
    else:
        pattern = r"(&RAN\(T\d+[,A-Z]*\)|&RAN\(N\d+[,0-9]*\)|&RAN\(t\d+[,a-z]*\))|(&RAN\[\s*'(.*)'(?:\s*,\s*'(.*)')*\s*])"
        matches = re.findall(pattern, src)
        if matches is not None:
            matches = flattenlist(matches)
            #dprint(f"rran matches={matches}", DEBUG_INFO)
            for m in matches:
                #dprint(f"\tmatch={m}", DEBUG_INFO)
                if m.startswith('&RAN('):
                    n = ""
                    m1 = f"[{m[6:].rstrip(')')}]"
                    L = 0
                    F = None
                    T = None

                    m1 = m[6:].rstrip(')')
                    #dprint(m1)
                    c = m1.find(",")
                    if c == -1: L = int(m1)
                    else:
                        L = int(m1[:c])
                        m1 = m1[c+1:]
                        c = m1.find(",")
                        if c == -1: F = m1[c]
                        else:
                            F = m1[:c]
                            m1 = m1[c+1:]
                            c = m1.find(",")
                            if c == -1: T = m1[c]
                            else:
                                dprint(f"rran error for '{src}'", DEBUG_ERR)

                    if m.startswith('&RAN(N'):
                        # 0 - 9 (48-57)
                        F = 48 if F is None else min(57, max(48, ord(F)))
                        T = 57 if T is None else min(57, max(48, ord(T)))
                    elif m.startswith('&RAN(T'):
                        # A-Z (65-90)
                        F = 65 if F is None else min(90, max(65, ord(F)))
                        T = 90 if T is None else min(90, max(65, ord(T)))
                    elif m.startswith('&RAN(t'):
                        # a-z (97-122)
                        F = 97 if F is None else min(122, max(97, ord(F)))
                        T = 122 if T is None else min(122, max(97, ord(T)))
                    else:
                        dprint(f"rran error: {m}", DEBUG_ERR)

                    if T < F: T, F = F, T
                    L = min(80, max(1, L))

                    for i in range(L):
                            n += chr(random.randint(F, T))

                    src = re.sub(re.escape(m), n, src, count=1)
                    dprint(f"\treplaced '{m}' with '{n}'", DEBUG_INFO)
                elif m.startswith('&RAN['):
                    pattern = re.compile(r"&RAN\[\s*'(.*)'(?:\s*,\s*'(.*)')*\s*]")
                    list_matches = re.findall(pattern, src)
                    #dprint(f"rran matches={matches} for '{s}'")
                    random_list = []
                    if list_matches is not None:
                        list_matches = flattenlist(list_matches)
                        #dprint(f"processing random list {list_matches}", DEBUG_INFO)
                        for lm in list_matches:
                            if lm is not None:
                                if len(lm) > 0:
                                    random_list = ast_literal_eval(f"['{lm}']")
                                    dprint(f"random_list={random_list}", DEBUG_INFO)
                                    break

                        r = len(random_list)-1
                        if r >= 0:
                            n = random.randint(0, r)
                            newval = random_list[n]
                        else:
                            newval = ''

                        src = re.sub(re.escape(m), newval, src, count=1)
                        dprint(f"\treplaced '{m}' with '{newval}'", DEBUG_INFO)
                else:
                    dprint(f"not going to process match '{m}'", DEBUG_INFO)

    return src

##########################################################################
# DEBUG Capture
##########################################################################
debug_messages = []
RTN['debugfile'] = DEBUG_FILE
debug_messages.append(f"LOCAL_TIME={LOCAL_TIME.strftime('%d-%m-%y %H:%M.%S %z')}, UTC_TIME={UTC_TIME.strftime('%d-%m-%y %H:%M.%s')}, LOG_TIME={LOG_TIME}")
debug_messages.append(f"sys.argv={str(sys.argv)}")
debug_messages.append(f"DEBUG_INFO={DEBUG_INFO}, DEBUG_MSG={DEBUG_MSG}, DEBUG_ERR={DEBUG_ERR}, DEBUG_NONE={DEBUG_NONE}, DEBUG={DEBUG}, DEBUG_LEVEL={DEBUG_LEVEL}")
debug_messages.append(f"DEBUG_FILE={DEBUG_FILE}, DEBUG_DIR={DEBUG_DIR}, DEBUG_OUT_PREFIX={DEBUG_OUT_PREFIX}")
debug_messages.append(f"CHUNK_MB={CHUNK_MB}, CHUNK_PCT={CHUNK_PCT}")
debug_messages.append(f"ALLOWED_FLAGS={ALLOWED_FLAGS}, CONSTANT_FLAGS={CONSTANT_FLAGS}")
debug_messages.append(f"CONSTANT_COMMENT_CHAR={CONSTANT_COMMENT_CHAR}, CONSTANT_SEPERATOR_DEFAULT={CONSTANT_SEPERATOR_DEFAULT}, ALLOW_LOGFILE_DELETE={ALLOW_LOGFILE_DELETE}, ALLOW_HEADERS={ALLOW_HEADERS}")
debug_messages.append(f"TEMP_LOGFILENAME={TEMP_LOGFILENAME}")
debug_messages.append(f"TIMEOUT_SECS={TIMEOUT_SECONDS}, STATUS_OK={STATUS_OK}, STATUS_BAD_REQUEST={STATUS_BAD_REQUEST}, STATUS_BAD_PERMISSIONS={STATUS_BAD_PERMISSIONS}")
debug_messages.append(f"STATUS_BAD_TIMEOUT={STATUS_BAD_TIMEOUT}, STATUS_FILE_NOT_FOUND={STATUS_FILE_NOT_FOUND}, STATUS_BAD_MAPPINGS={STATUS_BAD_MAPPINGS}, STATUS_BAD_EXCEPTION={STATUS_BAD_EXCEPTION}")

parser = argparse.ArgumentParser()

# To stick with a requirement of Python 3.6, we cannot use the argparse.exit_on_error (3.9)
# therefore we must trap our own errors and quit safely before handing control over to Argparse
# that does its own quit that does not conform to the LogMasker out format
param_dict = {
    #'append': {'add_argument':['-a', '--append'],'default': False, 'action': 'store_true', 'help': 'Append masked log file to existing file or write over existing file. Default is not to append'},
    'logfile': {'add_argument': ['-l', '--log'], 'required': False, 'help': 'Source log file to process', 'v': True},
    'maskfile': {'add_argument': ['-m', '--mask'], 'required': False, 'help': 'Mask file containing masking rules to mask log contents'},
    'mapfile': {'add_argument': ['-p', '--map'], 'required': False, 'help': 'The file to store the mapping results so that lookups and restores can be performed. No value means no restore is possible'},
    #'restore_using_mapfile': {'add_argument': ['-r', '--restore'], 'required': False, 'help': 'Restore a masked log file back to its original state using the generated mapping file'},
    'newlogfile': {'add_argument': ['-n', '--newlog'], 'required': False, 'help': 'Specify a new file name, otherwise the log file is overwritten'},
    'out': {'add_argument': ['-o', '--out'], 'default': 'stdout', 'help': "Return output to ['stdout'], 'screen', 'none'. The 'stdout' will return a JSON object"},
    #'json': {'add_argument': ['-j', '--json'], 'default': False, 'action': 'store_true',  'help': 'Return output as JSON, otherwise string(s) are returned'},
    'delete_logfile': {'add_argument': ['-d', '--deletelog'], 'default': False, 'action': 'store_true', 'help': 'Delete the source input file'},
    #'test': {'add_argument': ['-t', '--test'], 'default': False, 'action': 'store_true',  'help': 'Test only. Does not create the mapfile or the newlogfile'},
    'logline': {'add_argument': ['-L', '--logline'], 'required': False, 'help': 'Supply a single log line string that will be subject to masking'},
    'maskline': {'add_argument': ['-M', '--maskline'], 'required': False, 'help': 'Supply a masking line entry to process the input (--logline must be supplied)'},
    #'chunk_mb': {'add_argument': ['--chunkmb'], 'default': CHUNK_MB,  'type': int, 'help': f"The maximum number of MB per chunk processed. Default is {CHUNK_MB:,}. Each chunk finishes at the end of the next line."},
    'mapping_seperator': {'add_argument': ['--mapseperator'], 'default': CONSTANT_SEPERATOR_DEFAULT, 'required': False, 'help': f"The characters to seperate the original to masked values in the mapping output. Default is {CONSTANT_SEPERATOR_DEFAULT}   Needs to be unique to allow restore to operate"},
    'headers': {'add_argument': ['--nofileheaders'], 'default': True, 'action': 'store_false', 'help': 'If parsed, no information about LogMasker will be inserted as comments in the header of the new logfile'},
    'comment_char': {'add_argument': ['--comment'], 'default': CONSTANT_COMMENT_CHAR, 'help': f"The characters to identify a comment line in the new files. Default is {CONSTANT_COMMENT_CHAR}, if an empty string is parsed, no comments are made"},
    'debug': {'add_argument': ['--debug'], 'default': False, 'action': 'store_true', 'help': 'If parsed, screen messages will be verbose of all masking performed and a debug mapping file will be produced'},
}

lp = [x.lower() for x in sys.argv]
ls = len(sys.argv)-1
i = 0
all_params = flattenlist([v['add_argument'] for v in param_dict.values()])
cmd_lookup = {arg: key for key, value in param_dict.items() for arg in value['add_argument'] if key != value}
parsed_params = {item: 0 for item in cmd_lookup.values()}
skip = False
param_stop = False

for x in lp:
    i += 1
    nx = '' if ls < i else lp[i]
    if i > 1:
        if x.startswith('-'):
            skip = True
            if parsed_params.get(cmd_lookup.get(x, '_'), 0) > 0:
                # duplicate parameter passed. Argparse will ignore, so will we
                continue

            if x not in cmd_lookup:
                if nx.startswith('-') or nx == '':
                    parser.add_argument(x, default=False, action='store_true', dest=f"bad_bool_param_{x}", help='Ignore. Manually added to avoid argparse issues < v3.9')
                    skip = False
                    cmd_lookup[x] = True
                    parsed_params[x] = 1
                    debug_messages.append(f"added bool parameter {x}")
                else:
                    parser.add_argument(x, required=False, dest=f"bad_str_param_{x}", help='Ignore. Manually added to avoid argparse issues < v3.9')
                    cmd_lookup[x] = nx
                    parsed_params[x] = 1
                    debug_messages.append(f"added str parameter {x}, i={i}, ls={ls}, nx='{nx}'")

                RTN['exceptions'].append(f"Unknown parameter parsed to LogMasker '{x}'")
        else:
            if skip:
                skip = False
            else:
                RTN['exceptions'].append(f"Unknown value parsed to LogMasker '{sys.argv[i-1]}'")
                param_stop = True
                break
    elif ls == 0:
        RTN['exceptions'].append("No parameters parsed to LogMasker")
        param_stop = True


if param_stop:
    i = -1
    out = "screen"
    if '-o' in lp:
        i = lp.index('-o')
    elif '--out' in lp:
        i = lp.index('--out')

    if i >= 0 and len(lp) > i:
        if lp[i+1] == 'stdout': out = "stdout"

    quit(out=out, exit_code=-1)
else:
    debug_messages.append("dictionary parameter approach has passed, proceeding ...")

# Build the Argparse object using the parameter dictionary that has been defined and potentially updated
for p in param_dict:
    switch_count = len(param_dict[p].get('add_argument', []))
    if switch_count == 2:
        parser.add_argument(param_dict[p].get('add_argument', [])[0], param_dict[p].get('add_argument', [])[1],
                        default=(param_dict[p].get('default', None)),
                        required=(param_dict[p].get('required', False)),
                        action=(param_dict[p].get('action', None)),
                        dest=p,
                        help=param_dict[p].get('help', None)
                        )
    elif switch_count == 1:
        parser.add_argument(param_dict[p].get('add_argument', [])[0],
                        default=(param_dict[p].get('default', None)),
                        required=(param_dict[p].get('required', False)),
                        action=(param_dict[p].get('action', None)),
                        dest=p,
                        help=param_dict[p].get('help', None)
                        )
    else:
        RTN['exceptions'].append(f"Parameter '{p}' has either none or too many switches defined")
        i = -1
        out = "screen"
        if '-o' in lp:
            i = lp.index('-o')
        elif '--out' in lp:
            i = lp.index('--out')

        if i >= 0 and len(lp) > i:
            if lp[i+1] == 'stdout': out = "stdout"

        quit(out=out, exit_code=-1)


param = parser.parse_args()

logfile = ""
source = []
sourcelines = 0
mapfile = ""
maplines = 0
re_params = []
finds = set() # stores the unique find values
repls = set()
fcnt = 0    # How many unique find values there are
masks = []
newval = ""
newfile = ""
maskcount = 0
max_original_val_len = 0
re_flags = 0


############################################################
## Validate parameters
## Check everything possible and append messages to a list.
## If any messages exist, print and quit.
############################################################

if param.debug or DEBUG:
    DEBUG = True
    DEBUG_LEVEL = min(DEBUG_MSG, DEBUG_LEVEL)
else:
    DEBUG = False
    DEBUG_LEVEL = DEBUG_ERR

if DEBUG:
    dl = f"\n{DEBUG_OUT_PREFIX.get(DEBUG_MSG, '')}"
    dprint(dl.join(debug_messages), DEBUG_MSG)
else:
    RTN['debugfile'] = None

# logfiles (log files) are read in chunks of a min of 1MB of characters in lines so that the search & replace processes are efficient
try:
    CHUNKCHARS = int(CHUNK_MB)*MB1
except:
    RTN['exceptions'].append("Localization CHUNK_MB was not a valid integer")

CHUNKCHARS = max(MB1, min(MB1*200 if CHUNKCHARS > MB1*200 else CHUNKCHARS, CHUNKCHARS))
CHUNK_PCT = max(0, min(60 if CHUNK_PCT > 60 else CHUNK_PCT, CHUNK_PCT))

seperator = '::::' if param.mapping_seperator is None else param.mapping_seperator
restore_seperator = ''

cflag_set = {ALLOWED_FLAGS[item] for item in CONSTANT_FLAGS if item in ALLOWED_FLAGS}

if param.out.lower() not in ('screen', 'none', 'stdout'):
    RTN['exceptions'].append(f"Invalid value passed to --out of '{param.out}'. Valid values are ['stdout'], 'screen', 'none'. Use 'stdout' for API Calls of single value returns")
    param.out = 'stdout'

if param.restore_using_mapfile is None:
    restore = False
    if len(seperator.strip()) < 2:
        RTN['exceptions'].append(f"Bad --mapseperator specified, please enter a unique value of at least 2 characters")
else:
    restore = ENABLE_RESTORE

if restore:
    RTN['exceptions'].append("Restore has been requested. This version of LogMasker cannot restore to the original log file")
    quit(param.out, -1)
    # if 1 == 2:
    #     if os.path.exists(param.restore_using_mapfile):
    #         with open(param.restore_using_mapfile, 'r') as f:
    #             RTN['mapfile'] = param.restore_using_mapfile
    #             if param.mapping_seperator is not None:
    #                 restore_seperator = seperator
    #             else:
    #                 # Get seperator from 1st line of mapping file, its ok if it has been manually moved
    #                 for line in f:
    #                     if line.startswith("#"):
    #                         sep_start = line.find("seperator=")
    #                         if sep_start > 0:
    #                             restore_seperator = line[sep_start+10:]
    #                             break

    #         if restore_seperator == '':
    #             RTN['exceptions'].append(f"Cannot locate seperator in masking map file '{param.restore_using_mapfile}' and --mapseperator not specified")
    #             RTN['status'] = STATUS_BAD_REQUEST
    #         else:
    #             with open(param.restore_using_mapfile, 'r') as f:
    #                 for line in f:
    #                     if len(line) > 1 and not line.startswith("#"):
    #                         ln = line.split(restore_seperator)
    #                         if len(ln) == 2:
    #                             re_params.append([ln[0].strip(), ln[1].strip[1]])
    #                         else:
    #                             dprint(f"ignoring mapfile line '{line}'")

    #             if len(re_params) == 0:
    #                 RTN['exceptions'].append(f"No masking map lines to restore in '{param.restore_using_mapfile}'")
    #                 RTN['status'] = STATUS_BAD_MAPPINGS if RTN['status'] == STATUS_OK else RTN['status']

    #     else:
    #         RTN['exceptions'].append(f"Invalid masking map file '{param.restore_using_mapfile}'")
    #         RTN['status'] = STATUS_FILE_NOT_FOUND if RTN['status'] == STATUS_OK else RTN['status']

    #     if param.logline is not None:
    #         RTN['exceptions'].append(f"The --logline parameter cannot be parsed as part of a restore")
    #         RTN['status'] = STATUS_BAD_REQUEST if RTN['status'] == STATUS_OK else RTN['status']

    #     if param.maskline is not None:
    #         RTN['exceptions'].append(f"The --maskline parameter cannot be parsed as part of a restore")
    #         RTN['status'] = STATUS_BAD_REQUEST if RTN['status'] == STATUS_OK else RTN['status']

if not restore:
    if param.logline is not None:
        source.append(param.logline)

        if param.logfile is not None:
            RTN['exceptions'].append("You cannot specify both --logline and --log to mask")
            RTN['status'] = STATUS_BAD_REQUEST if RTN['status'] == STATUS_OK else RTN['status']

    elif param.logfile is None:
        RTN['exceptions'].append("You must supply either a --logline or --log to be masked")
        RTN['status'] = STATUS_BAD_REQUEST if RTN['status'] == STATUS_OK else RTN['status']


# The logfile is loaded for both masking and restores
if param.logfile is not None:
    if os.path.exists(param.logfile):
        i = 0
        with open(param.logfile, 'r') as f:
            chunk = f.readlines(CHUNK_MB)
            while chunk:
                i += 1
                if i % 100 == 0:
                    if is_timeout(f"logfile chunk {i}"): quit(param.out, -1)
                source.append("".join(chunk))
                chunk = f.readlines(CHUNKCHARS)

            # If the last chunk is < CHUNK_PCT% of the prior chunk, we add it to the prior chunk
            # so that the looping to a small chunk of text is avoided
            ls = len(source)
            if ls > 1:
                ls1, ls2 = len(source[ls-2]), len(source[ls-1])
                lc = int((ls2/ls1)*100)
                if lc < CHUNK_PCT:
                    source[ls-2] += source[ls-1]
                    source.pop(-1)
                    dprint(f"appending last chunk as it is less than {CHUNK_PCT*100}% of the prior chunk ({int(lc)*100}%)", DEBUG_INFO)
    else:
        RTN['exceptions'].append(f"The logfile supplied is not a valid file - '{param.logfile}'")
        RTN['status'] = STATUS_FILE_NOT_FOUND if RTN['status'] == STATUS_OK else RTN['status']

if not restore:
    maskline_filename = None
    if param.maskline is None and param.maskfile is None:
            RTN['exceptions'].append("You must supply either a --maskline or --maskfile parameter value to use for masking rules")
            RTN['status'] = STATUS_BAD_REQUEST if RTN['status'] == STATUS_OK else RTN['status']
    elif param.maskline is not None:
        # Need to pass this through literal_val and append with the maskfile linenumber and uniqueness test
        maskfile_filename = f"maskline-{LOCAL_TIME.strftime('%y%m%d%H%M%S%f')}-temp.maskline"
        with open(maskfile_filename, 'w') as file:
            file.write(param.maskline + '\n')
        file.close()
    elif param.maskfile is not None:
        maskfile_filename = param.maskfile

    if maskfile_filename is not None:
        dprint(f"loading maskfile {param.maskfile}", DEBUG_MSG)
        if os.path.exists(maskfile_filename):
            bad_maskfile_lines = []
            with open(maskfile_filename, 'r') as f:
                linenumber = 0
                for line in f:
                    linenumber += 1
                    if linenumber % 10000 == 0:
                        if is_timeout(f"maskfile line {linenumber}"): quit(out.param, -1)

                    if len(line) > 1 and not line.startswith("#"):
                        if line.strip().replace(' ', '').startswith('flags='):
                            # remove spaces
                            ln = [line.strip().replace(' ', '')]
                        else:
                            try:
                                ln = ast_literal_eval(line)
                            except:
                                ln = []
                                bad_maskfile_lines.append([f"error line {linenumber}:", line.strip()])
                                continue

                        if len(ln) == 2:
                            re_params.append([ln[0].strip(), ln[1].strip(), linenumber, ln[1].find('&RAN') > -1])
                        elif len(ln) == 1:
                            if ln[0].startswith('flags='):
                                fln = ln[0].split('=')
                                if len(fln) == 2:
                                    flags = fln[1].strip().replace('re.','').split(",")
                                    flags = [item[:1].upper() for item in flags]
                                    bad_flags = [item for item in flags if item not in ALLOWED_FLAGS]
                                    if len(flags) == 0:
                                        ln = []
                                        bad_maskfile_lines.append([f"no flags {linenumber}:", line.strip()])
                                        continue
                                    elif bad_flags:
                                        ln = []
                                        bad_maskfile_lines.append([f"bad flags {linenumber}:", f"bad flags {bad_flags} in: {line.strip()}"])
                                        continue
                                    elif len(flags) > 1 and '0' in flags:
                                        ln = []
                                        bad_maskfile_lines.append([f"too many flags {linenumber}:", line.strip()])
                                        continue
                                    else:
                                        #if 'V' in flags: flags.append('X')
                                        flag_set = {ALLOWED_FLAGS[item] for item in flags if item in ALLOWED_FLAGS}
                                        re_params.append([RE_FLAGS_ID, flag_set, linenumber, 0])
                                        dprint(f"good flags line: {flags}",0)
                                else:
                                    bad_maskfile_lines.append([f"bad flags line {linenumber}:", line.strip()])
                            else:
                                bad_maskfile_lines.append([f"unknown line {linenumber}:", line.strip()])
                        else:
                            bad_maskfile_lines.append([f"bad format line {linenumber}:", line.strip()])

            if len(bad_maskfile_lines) > 0:
                RTN['masklines_bad'] = len(bad_maskfile_lines)
                RTN['status'] = STATUS_BAD_MAPPINGS if RTN['status'] == STATUS_OK else RTN['status']
                RTN['exceptions'].append(f"The maskfile supplied has {len(bad_maskfile_lines)} errors that must be fixed before masking can proceed:")
                for line in bad_maskfile_lines:
                    RTN['exceptions'].append(f" - {line[0]} {line[1]}")
        else:
            RTN['exceptions'].append(f"The maskfile supplied is not a valid file - '{maskfile_filename}'")
            RTN['status'] = STATUS_FILE_NOT_FOUND if RTN['status'] == STATUS_OK else RTN['status']

        if maskfile_filename.endswith('-temp.maskline'):
            if os.path.exists(maskfile_filename):
                try:
                    os.remove(maskfile_filename)
                except Exception as e:
                    err_msg = f"Masking Line temporary file '{maskfile_filename}' could not be deleted - {str(e)}"
                    dprint(err_msg, DEBUG_ERR)
                    RTN['status'] = STATUS_BAD_PERMISSIONS if RTN['status'] == STATUS_OK else RTN['status']
                    RTN['exceptions'].append(err_msg)
                    RTN['exceptions'].append(f"{traceback.format_exc()}")
                    quit(param.out, -1)

if len(RTN['exceptions']) > 0:
    RTN['status'] = STATUS_BAD_REQUEST if RTN['status'] == STATUS_OK else RTN['status']
    quit(param.out, -1)
else:
    dprint("no validation errors were found... proceeding", DEBUG_MSG)


##################################
## Start masking
##################################
dprint(f"source chunks = {len(source)}", DEBUG_INFO)
debug_mapping_lines = []
create_debug_mapping_file = True if param.mapfile is not None and DEBUG else False
if not restore:
    operation_count = 0
    flags_linenumber = 0
    non_unique_rules = []
    if len(re_params) == 0:
        # get regexs from maskfile file ignoring comments and empty lines
        dprint("len(re_params)=0", DEBUG_MSG)

    re_param_i = 0
    for re_param_line in re_params:
        re_param_i += 1
        if re_param_i % 100 == 0:
            if is_timeout(f"Sequential Masking Line {re_param_i}"): quit(param.out, -1)

        if re_param_line[0] == RE_FLAGS_ID:
            # process the re_flags variable
            flag_set = cflag_set | re_param_line[1]
            re_flags = 0
            flags_linenumber = re_param_line[2]
            for item in flag_set:
                re_flags |= item

            dprint(f"Mask File Line {flags_linenumber}: re_flags have been set to {re_flags}", DEBUG_MSG)
            #if create_debug_mapping_file: debug_mapping_lines.append(f"Mask File Line: {flags_linenumber} has set regex flags to {re_flags}")
            if create_debug_mapping_file: debug_mapping_lines.append(['F', flags_linenumber, re_flags])
        else:
            try:
                matchset = set()
                for s in source:
                    operation_count += 1
                    if operation_count % 100 == 0:
                        if is_timeout(f"Regex Find Operation {operation_count}"): quit(param.out, -1)

                    raw_matchlist = re.findall(re_param_line[0], s, flags=re_flags)
                    matchset = set(flattenlist(raw_matchlist))|matchset
            except Exception as e:
                msg = f"masking line {re_param_line[2]} has encountered an error. re_param_line={', '.join(re_param_line[0:2])}"
                dprint(msg, DEBUG_ERR)
                RTN['exceptions'].append(msg)
                RTN['exceptions'].append(f"{traceback.format_exc()}")
                RTN['masklines_bad'] += 1
                if create_debug_mapping_file: debug_mapping_lines.append(['E', re_param_line[2], re_param_line[0], re_param_line[1], -1])
                continue


            # It is important that the largest find values are replaced first to avoid complications with smaller
            # values containing the same text as the larger values.
            # for example 'server129' should be replaced before 'server1', otherwise we will end up with the mapping that
            # states 'server1  ::::  svr55' and when looking in the masked logfile, we see 'svr5529' which could
            # lead to confusion
            mc = len(matchset)
            if create_debug_mapping_file: debug_mapping_lines.append(['L', re_param_line[2], re_param_line[0], re_param_line[1], mc])
            if mc > 0:
                re_param_line[3] = mc
                matchlist = sorted(list(matchset), key=lambda x: (-len(x), x))
                RTN['masklines_used'] += 1
            else:
                matchlist = []
                RTN['masklines_zero'] += 1

            dprint(f"Mask File Line {re_param_line[2]}: re_param_line={re_param_line[1:2]} - matchlist={re_param_line[3]} items", DEBUG_MSG)

            for m in matchlist:
                # We need to ensure that each find value is unique and is only replaced once
                #Have a new criteria to remove line &REMOVELINE(leading_lines=0, trailing_lines=0) --> to do this we need to find the line
                #Have a new criteria to replace entire line with new text &NEWLINE(repl='', leading_lines=0, trailing_lines=0) --> trailing and leading are removed
                finds.add(m)
                if len(finds) > fcnt:
                    fcnt += 1
                    if len(m) > max_original_val_len:
                        max_original_val_len = len(m)

                    i = 0
                    last_err_i = 0
                    performed_repl = False
                    # Only proceed if the replaced value is also unique in source
                    while not performed_repl:
                        rval = rran(re_param_line[1])
                        newmatch = re.sub(m, rval, m, flags=re.IGNORECASE|re.VERBOSE)
                        dprint(f"     set newmatch='{newmatch}', i={i}, maskline={re_param_line[2]}", DEBUG_INFO)
                        found_in_chunk = False
                        # check that newmatch does not exist inside source
                        for s in source:
                            match = re.search(newmatch, s, re.IGNORECASE)
                            if match:
                                # Allow the duplicate match to proceed if no mapping file is required on the 3rd attempt at uniqueness
                                if param.mapfile is None and i > 2:
                                    found_in_chunk = False
                                    dprint(f"     non-unique rule detected: m={m}, newmatch={newmatch}, i={i}, maskline={re_param_line[2]}", DEBUG_MSG)
                                    non_unique_rules.append([re_param_line[0], re_param_line[1], m, newmatch, re_param_line[2]])
                                    if create_debug_mapping_file: debug_mapping_lines.append(['N', re_param_line[2], m, newmatch, -1])
                                    continue
                                else:
                                    found_in_chunk = True
                                    dprint(f"     - found a duplicate match, i={i}, maskline={re_param_line[2]}", DEBUG_INFO)

                        if not found_in_chunk:
                            r_count = len(repls)
                            repls.add(newmatch)
                            if len(repls) > r_count or param.mapfile is None:
                                ml = len(newmatch)
                                x_nm = sorted(list(repls), key=lambda x: (-len(x), x))
                                for x in x_nm:
                                    if len(x) > ml:
                                        if x.lower().find(newmatch.lower()) > -1 and param.mapfile is not None:
                                            found_in_chunk = True
                                            dprint(f"     - found in part of repls value, i={i}, maskline={re_param_line[2]}", DEBUG_INFO)
                                            break
                                    else:
                                        break

                                if not found_in_chunk:
                                    masks.append([m, newmatch, re_param_line[2]])
                                    if create_debug_mapping_file: debug_mapping_lines.append(['M', re_param_line[2], m, newmatch, 0])
                                    dprint(f"    - original '{m}' will be replaced with '{newmatch}'", DEBUG_INFO)
                                    performed_repl = True
                            else:
                                found_in_chunk = True
                                dprint(f"     - newmatch already exists in repls set '{newmatch}', i={i}, maskline={re_param_line[2]}", DEBUG_INFO)

                        if found_in_chunk and i > 8:
                            dprint(f"     - unique repl loop > 8, cancelling. Latest val={newmatch}, maskline={re_param_line[2]}", DEBUG_MSG)
                            RTN['exceptions'].append(f"Mask File Line {re_param_line[2]}: {re_param_line[:1]} - has {len(matchlist)} matching item/s, of which at least 1 has a non-unique replacement")
                            RTN['exceptions'].append(f"Mask File Line {re_param_line[2]}: found '{m}' but cannot establish a unique replacement for masking - '{newmatch}' as it already exists. Masking abandoned")
                            if create_debug_mapping_file: debug_mapping_lines.append(['C', re_param_line[2], m, newmatch, -1])
                            break
                        elif found_in_chunk:
                            i += 1
                        elif i > 12:
                            # catch-all in case of logic error to avoid endless loop
                            RTN['exceptions'].append(f"Mask File Line {re_param_line[2]}: catch-all: original {m} cannot establish a unique replacement for mapping - {newmatch}. Masking abandoned")
                            continue
                else:
                    dprint(f" - Mask File Line {re_param_line[2]}: ignored find value as it is not unique: '{m}'", DEBUG_MSG)


    RTN['mappings'] = len(masks)
    if RTN['mappings'] == 0 and param.mapfile is not None: RTN['exceptions'].append("No masking rules found. Mapping file cannot be built")
    if len(RTN['exceptions']) > 0:
        RTN['status'] = STATUS_BAD_MAPPINGS if RTN['status'] == STATUS_OK else RTN['status']
        quit(param.out, -1)
    elif len(source) == 0:
        RTN['exceptions'].append("No source log text or log file to process")
        RTN['status'] = STATUS_NO_SOURCE if RTN['status'] == STATUS_OK else RTN['status']
        quit(param.out, -1)

    sorted_masks = sorted(masks, key=lambda x: (-len(x[1]), x))
    i = 0
    for mline in sorted_masks:
        i += 1
        if i % 1000 == 0:
            if is_timeout(f"Masking Sub Operation {i}"): quit(param.out, -1)
        new_source = []
        while len(source) > 0:
        #for s in source:
            i += 1
            if i % 1000 == 0:
                if is_timeout(f"Masking Sub Operation {i}"): quit(param.out, -1)

            new_source.append(re.sub(mline[0], mline[1], source.pop(0), flags=0))

        source = new_source


# NOTE --> WE NO LONGER CHECK TIMEOUTS HEREIN BECAUSE WE ARE CREATING FILES.
    # We only create a mapping file if one is specified
    if param.mapfile is not None:
        mc = 0
        with open(param.mapfile, 'w') as file:
            RTN['mapfile'] = param.mapfile
            # iterate over the chunks and write each one to the file
            mc = len(masks)
            dprint(f"{'nothing to write ' if mc == 0 else 'writing '} to mask file '{param.mapfile}', mask entries = {mc}", DEBUG_MSG)
            if mc > 0:
                file.write(f"{param.comment_char} {mc} mapping entries created by LogMasker for {param.logfile} on {LOCAL_TIME}, seperator={seperator}\n{param.comment_char}\n")
                omasks = sorted(masks, key=lambda x: x)
                for m in omasks:
                    spaces = max_original_val_len + 4 - len(m[0])
                    line = f"{m[0]}{' '*spaces}{seperator}{' '*4}{m[1]}\n"
                    file.write(line)
                file.write(f"{param.comment_char}\n{param.comment_char} {mc} mapping entries written to file\n")
        file.close()

    if param.logline is not None: RTN['logline_out'] = "".join(source)


##################################
## Start processing restore
##################################

elif restore:
    RTN['exceptions'].append("restore function not yet coded")



##################################
## Create New files
##################################

# Write logfile
if not restore and param.logfile is not None:
    # Make the tempfilename a localisation
    newlogfile = param.newlogfile if param.newlogfile is not None else TEMP_LOGFILENAME
    RTN['newlogfile'] = newlogfile
    try:
        with open(newlogfile, 'w') as file:
            # iterate over the chunks and write each one to the file
            if param.headers and ALLOW_HEADERS:
                file.write(f'{param.comment_char} This file has been masked by LogMasker from {param.logfile} on {LOG_TIME} using masking rules from {param.maskfile}\n')

            for chunk in source:
                file.write(chunk)
        # close the file
        file.close()
        if param.newlogfile is None:
            os.rename(newlogfile, param.logfile)
            RTN['newlogfile'] = param.logfile

        dprint(f"Masked LogFile '{newlogfile if param.newlogfile else param.logfile}' created", DEBUG_MSG)
    except Exception as e:
        RTN['status'] = STATUS_BAD_PERMISSIONS
        RTN['exceptions'].append(f"Cannot write to new logfile '{newlogfile}' - {str(e)}")


    #    {re_param_line[0]}{spaces}{seperator}    {re_param_line[1]}    --> in alphabetical order of FROM
    # debug_maaping_lines format = [C/Cancelled|E/Error|F/Flag|L/Line|M/Match|N/Non-Unique, Mask File Line Number,  from|flags|m,  to|newmatch, count ]
    if len(debug_mapping_lines) > 0:
        debug_mapfile = f"{param.mapfile}.debug"
        with open(debug_mapfile, 'w') as file:
            RTN['debugmapfile'] = debug_mapfile
            dprint(f"debug_mapfile '{debug_mapfile}' created", DEBUG_MSG)
            file.write(f"{param.comment_char} LogMasker Masking Debug file for {param.logfile} on {LOCAL_TIME}, seperator={seperator}\n" +
                       f"{param.comment_char} Debug Masking Lines:         {len(debug_mapping_lines)}\n" +
                       f"{param.comment_char} Masking File Used:           {param.mapfile}\n" +
                       f"{param.comment_char} Masked Log File:             {newlogfile}\n" +
                       f"{param.comment_char} Errored Lines denoted by:    !!   --> this means the regex expression failed parsing and needs correction in the masking file\n\n\n")

            maplines = []
            for line in debug_mapping_lines:
                wline = ""
                spaces = 0 if line[0] in ('F', 'L') else max_original_val_len + 4 - len(line[2])
                if line[0] == 'C':
                    maplines.append([line[2], f" *  {line[2]}{' '*spaces}{seperator}{' '*4}{line[3]}\n"])
                elif line[0] == 'E':
                    maplines.append([line[2], f" !! {line[2]}{' '*spaces}{seperator}{' '*4}{line[3]}\n"])
                elif line[0] == 'F':
                    maplines = sorted(maplines, key=lambda x: (x[0]))
                    ll = [x[1] for x in maplines]
                    wline = ''.join(ll) + f"\nMask Line: {line[1]} has set regex flags to {line[2]}\n"
                    file.write(wline)
                    maplines = []
                    wline = None
                elif line[0] == 'L':
                    maplines = sorted(maplines, key=lambda x: (x[0]))
                    ll = [x[1] for x in maplines]
                    wline = ''.join(ll) + f"\nMask Line: {line[1]} maps '{line[2]}'    to    '{line[3]}'   with {line[4]} matches\n"
                    file.write(wline)
                    maplines = []
                    wline = None
                elif line[0] == 'M':
                    maplines.append([line[2], f"    {line[2]}{' '*spaces}{seperator}{' '*4}{line[3]}\n"])
                #elif line[0] == 'N':
                #    wline = 'N  ' + str(line) + '\n'
                else:
                    wline = f"!! Unknown line type: {line[0]}, 1={line[1]}, 2={line[2]}, 3={line[3]}, 4={line[4]}\n"

            if len(maplines) > 0:
                maplines = sorted(maplines, key=lambda x: (x[0]))
                ll = [x[1] for x in maplines]
                wline = ''.join(ll)
                file.write(wline)
                maplines = []
                wline = None

            file.write('\n')

    else:
        dprint(f"Debug Mapping File not created because DEBUG={DEBUG} and debug masking lines count = {len(debug_mapping_lines)}", DEBUG_MSG)



# Write results and quit

quit(param.out, 0)
